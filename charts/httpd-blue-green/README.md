# HTTPD Blue/Green

This is a HTTPD chart with *blue/green* deployment ability. Blue/Green strategy is used
to minimize downtime for production services. When a new version has to be rolled out, it
is first deployed in the staging environment. When all testing is done, the live traffic
is moved to the staging environment that becom the new production environment. Fast rollback
can be done by changing route.


## Diagram

```
                                         +----------------+    +------------+
                                         | Svc Production |    | Slot Blue  |
                                         +----------------+    +------------+
                        www.domain.com
+----+    +---------+
| LB |    | Ingress |
+----+    +---------+
                      staging.domain.com
                                         +----------------+    +------------+
                                         | Svc Staging    |    | Slot Green |
                                         +----------------+    +------------+
```


## Release Process

Before deploying an new release, we need to read the current production slot.

```
RELEASE_NAME=my-release
CURRENT_SLOT=`(helm get values --all ${RELEASE_NAME} | grep -Po 'productionSlot: \K.*')`
if [ "${CURRENT_SLOT}" == "blue" ]; then
    NEW_SLOT="green"
else
    NEW_SLOT="blue"
fi
```

The new release will be deployed in the other slot. Note that the deployment slot must
not exist. If it exist, it must be disabled first

```
helm upgrade ${RELEASE_NAME} cburki/httpd-blue-green --set ${NEW_SLOT}.enabled=false --reuse-values
helm upgrade ${RELEASE_NAME} cburki/httpd-blue-green --set ${NEW_SLOT}.enabled=true --reuse-values
```

When the deployment slot exist, instead of removing and creating again the deployment, the
new deployment image can be set using kubernetes command line client.

```
kubectl set image deployment/htpd-blue-green-${NEW_SLOT} httpd-blue-green=<image>:<tag>
```

Once the new deployment has been tested and validated it can be promoted to production.

```
helm upgrade ${RELEASE_NAME} cburki/httpd-blue-green --set productionSlot=${NEW_SLOT} --reuse-values
```

This flip the slot labels on services with `Svc Production` pointing to `${NEW_SLOT}`. All
production traffic is now going tp the `${NEW_SLOT}`.


# References

This chart is based on the work of Puneet Saraswat.

https://github.com/puneetsaraswat/HelmCharts/tree/master/blue-green
https://medium.com/@saraswatpuneet/blue-green-deployments-using-helm-charts-93ec479c0282
