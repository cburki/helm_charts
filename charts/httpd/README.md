# HTTPD

This is a HTTPD chart that can be deployed in a cloud provider. The LoadBalancer component is optional
and must not be deployed in the case an ingress controller is available on your kubernetes cluster.

## Diagram

```

+----+  +---------+                +---------+  +-------+
| LB |  | Ingress | www.domain.com | Service |  | HTTPD |
+----+  +---------+                +---------+  +-------+

```
